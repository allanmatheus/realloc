#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KiB (1024)
#define MiB (1024 * KiB)
#define GiB (1024 * MiB)

struct state {
	void *m;
	size_t allocated;
	size_t block_size;
};

int do_alloc_block(struct state *s);
int do_free_block(struct state *s);
void print_state(struct state *s);

int exec_command(int c, struct state *s);

int isblank(int c);

void print_help();

int main()
{
	int c;
	struct state s = { .m = NULL, .allocated = 0, .block_size = 1 * GiB };
	print_help();
	printf("> ");
	while ((c = getchar()) != EOF) {
		int r;
		if (c == '\n')
			printf("> ");
		else {
			r = exec_command(c, &s);
			if (r < 0) {
				if (s.m)
					free(s.m);
				return r == -1;
			}
		}
	}
	return 0;
}

int do_alloc_block(struct state *s)
{
	size_t n = s->allocated + s->block_size;
	char *m = realloc(s->m, n);
	if (!m)
		return -1;
	memset(m, 0, n);
	s->m = m;
	s->allocated = n;
	return 0;
}

int do_free_block(struct state *s)
{
	size_t n = s->allocated > s->block_size
		? s->allocated - s->block_size
		: 0;
	char *m = realloc(s->m, n);
	if (!m)
		return -1;
	s->m = m;
	s->allocated = n;
	return 0;
}

int exec_command(int c, struct state *s)
{
	switch (c) {
		case '+':
			return do_alloc_block(s);
		case '-':
			return do_free_block(s);
		case '*':
			s->block_size *= 2;
			break;
		case '/':
			s->block_size /= 2;
			break;
		case 'q':
			return -2;
		case 'p':
			print_state(s);
			break;
		case '?':
			print_help();
			break;
		default:
			fprintf(stderr, "command '%c' not found\n", c);
			break;
	}
	return 0;
}

int isblank(int c)
{
	return c == ' ' || c == '\t' || c == '\n';
}

void print_state(struct state *s)
{
	printf("block size: %zu\ntotal size: %zu\n",
			s->block_size, s->allocated);
}

void print_help()
{
	fprintf(stderr, "commands:\n"
			" + alloc up to 1 block size of memory\n"
			" - free up to 1 block size of memory\n"
			" * double the block size\n"
			" / divide the block size by 2\n"
			" q quit\n"
			" p show current state\n"
			" ? print help\n");
}
